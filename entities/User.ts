import { Entity, Property} from '@mikro-orm/core';
import { BaseEntity } from './BaseEntity';

@Entity()
export class User extends BaseEntity {

  @Property()  
  firstName!: string;

  @Property()  
  lastName!: string;

  @Property()
  email: string;

  @Property()
  age: string;

  constructor(email: string, age: string) {
    super();
    this.email = email;
    this.age = age;
  }
  
}