import { User } from '../entities/User';
import { Options } from '@mikro-orm/core';

const options: Options = {
  entities: [ User ],  
  dbName: 'test-typescript',  
  type: 'mongo',
  debug: true,
};

export default options;