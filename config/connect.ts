import { EntityManager, EntityRepository, MikroORM, RequestContext } from '@mikro-orm/core';
import { User } from '../entities/User';

export const DI = {} as {
  orm: MikroORM,
  em: EntityManager,
  userRepository: EntityRepository<User>,
};

(async () => {
  DI.orm = await MikroORM.init();
  DI.em = DI.orm.em;
  DI.userRepository = DI.orm.em.getRepository(User);
})();