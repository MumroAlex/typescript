"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestGet = void 0;
const axios_1 = __importDefault(require("axios"));
class RequestGet {
    constructor(reqUrl, reqToken, reqParam) {
        this.url = reqUrl;
        this.token = reqToken;
        this.param = reqParam;
    }
    getWithoutParams() {
        return __awaiter(this, void 0, void 0, function* () {
            axios_1.default.defaults.headers.common = { 'Authorization': `Bearer ${this.token}` };
            axios_1.default.defaults.headers.get['Content-Type'] = 'application/json';
            try {
                const currentResult = yield axios_1.default.get(this.url);
                // console.log('Status:', currentResult.status);
                return currentResult.data;
            }
            catch (error) {
                console.error('Error:', error.response.data);
            }
        });
    }
    getWithParams() {
        return __awaiter(this, void 0, void 0, function* () {
            axios_1.default.defaults.headers.common = { 'Authorization': `Bearer ${this.token}` };
            axios_1.default.defaults.headers.get['Content-Type'] = 'application/json';
            try {
                const currentResult = yield axios_1.default.get(this.url, {
                    params: this.param
                });
                //console.log('Status:', currentResult.status);
                return currentResult.data;
            }
            catch (error) {
                console.error('Error:', error.response.data);
            }
        });
    }
}
exports.RequestGet = RequestGet;
