interface IBook {
  name: string;
  author: string;
  page: number;
  discription?: string;
}

class Book implements IBook {
  name: string;
  author: string;
  page: number;
  constructor(bookName: string, bookAuthor: string, bookPage: number) {
    this.name = bookName;
    this.author = bookAuthor;
    this.page = bookPage;
  }

  getBook(): string {
    return "My Book: " + this.name + "  Author: " + this.author + "  Page: " + this.page;
  }

}

export {Book};