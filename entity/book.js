"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Book = void 0;
class Book {
    constructor(bookName, bookAuthor, bookPage) {
        this.name = bookName;
        this.author = bookAuthor;
        this.page = bookPage;
    }
    getBook() {
        return "My Book: " + this.name + "  Author: " + this.author + "  Page: " + this.page;
    }
}
exports.Book = Book;
