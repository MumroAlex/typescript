interface IAnimal {
  name: string;
  age: number;
}

class Dog implements IAnimal {
 
  name: string;
  age: number;
  color: string;
  constructor(dogName: string, dogAge: number, dogColor: string) {
      this.name = dogName;
      this.age = dogAge;
      this.color = dogColor;
  }
  getDog(): string {
      return "My Dog (Name: " + this.name + " Age: " + this.age + " Color: " + this.color + ")";
  }
  getYearOfBirth(): string {
    const currentDate: number = new Date().getFullYear();
    const yearBirth: number = currentDate - this.age;
    return "Dog Year Of Birth: " + yearBirth;
  }
}

export {Dog};