import axios from 'axios';

interface IGetParams {
  url: string,
  token: string,
  param?: Object
}

class RequestGet implements IGetParams {
  url: string;
  token: string;
  param?: object;

  constructor (reqUrl: string, reqToken: string, reqParam?: object) {
    this.url = reqUrl;
    this.token = reqToken;
    this.param = reqParam;
  }

  async getWithoutParams(): Promise<any> {
    axios.defaults.headers.common = {'Authorization': `Bearer ${this.token}`};
    axios.defaults.headers.get['Content-Type'] = 'application/json';

      try {
        const currentResult: any = await axios.get(this.url);
        // console.log('Status:', currentResult.status);
        return currentResult.data;
      } catch (error: any) {
        console.error('Error:', error.response.data);
      }
  }
  async getWithParams(): Promise<any> {
    axios.defaults.headers.common = {'Authorization': `Bearer ${this.token}`};
    axios.defaults.headers.get['Content-Type'] = 'application/json';

      try {
        const currentResult: any = await axios.get(this.url, {
            params: this.param
          });
        //console.log('Status:', currentResult.status);
        return currentResult.data;
      } catch (error: any) {
        console.error('Error:', error.response.data);
      }
  }
}

export {
  RequestGet
 } 