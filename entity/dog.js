"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dog = void 0;
class Dog {
    constructor(dogName, dogAge, dogColor) {
        this.name = dogName;
        this.age = dogAge;
        this.color = dogColor;
    }
    getDog() {
        return "My Dog (Name: " + this.name + " Age: " + this.age + " Color: " + this.color + ")";
    }
    getYearOfBirth() {
        const currentDate = new Date().getFullYear();
        const yearBirth = currentDate - this.age;
        return "Dog Year Of Birth: " + yearBirth;
    }
}
exports.Dog = Dog;
