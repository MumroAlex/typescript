import { RequestGet } from "./entity/get";

//=== for business/v1/community-filter-users/ ===//
// const url: string = "http://127.0.0.1:9016/business/v1/community-filter-users/";
// const token: string = "d4fe64a4d1e35852641be2a98c560dd0aa3821d89c13d3eda5e6ccaae6584846b1c992237054fb0fce70f6fa1880f8a420b9ade6b2f6df5ff4108e7697b71554";
const url: string = "http://18.184.253.145:9016/business/v1/community-filter-users/";
const token: string = "38e7b36dc7d85d9f686d44fc49bd881f3582c07b224c89c1943e3af8c7322da159aa1e52ca67a36482087652d5729f5f57bb94a64de9a4cf8783e754978048ba";

//=== for /user/v1/list ===//
const urlUserList: string = "http://18.184.253.145:9016/user/v1/list";
const tokenUserList: string = "25d0737ac432055d7651c6d5aca3d8a241060042f85e3f3b7dd343d318ac1fd210e4339ae8e6310b564fb46159e802503cbbaf52fdd356f2a82868dd805dfcc1";
const paramUserList: object = {
  limit: 5,
  offset: 0,
  venueId: "5de65d89ec7e0e2f25045298",
  inVenue: true
}

const newCommunity = async () => {
    let getCommunity = new RequestGet(url, token);
    const resultWithOut = await getCommunity.getWithoutParams();
    console.log('result newCommunity', resultWithOut);
}

const newUserList = async () => {
  let getUserList = new RequestGet(urlUserList, tokenUserList, paramUserList);
  const resultWith = await getUserList.getWithParams();
  console.log('result User list', resultWith);
}

newCommunity();
newUserList();






