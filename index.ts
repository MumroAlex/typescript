import config from './config/mikro-orm.config';
import { User } from './entities/User';
import { EntityManager, EntityRepository, MikroORM, RequestContext } from '@mikro-orm/core';
import { DI } from './config/connect';
import { ExitStatus } from 'typescript';

const dataUsers = async () => {
  try {
    // const users = await DI.userRepository.find({lastName: "Saw"});
  // console.log('currentUsers - 890', users);
  const orm = await MikroORM.init(config);
  const usersTest = await orm.em.find(User, {});
  console.log('usersTest - 890', usersTest);
  } catch (error) {
    console.log('Error:', error);
  }
}

dataUsers();