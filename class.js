"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const get_1 = require("./entity/get");
//=== for business/v1/community-filter-users/ ===//
// const url: string = "http://127.0.0.1:9016/business/v1/community-filter-users/";
// const token: string = "d4fe64a4d1e35852641be2a98c560dd0aa3821d89c13d3eda5e6ccaae6584846b1c992237054fb0fce70f6fa1880f8a420b9ade6b2f6df5ff4108e7697b71554";
const url = "http://18.184.253.145:9016/business/v1/community-filter-users/";
const token = "38e7b36dc7d85d9f686d44fc49bd881f3582c07b224c89c1943e3af8c7322da159aa1e52ca67a36482087652d5729f5f57bb94a64de9a4cf8783e754978048ba";
//=== for /user/v1/list ===//
const urlUserList = "http://18.184.253.145:9016/user/v1/list";
const tokenUserList = "25d0737ac432055d7651c6d5aca3d8a241060042f85e3f3b7dd343d318ac1fd210e4339ae8e6310b564fb46159e802503cbbaf52fdd356f2a82868dd805dfcc1";
const paramUserList = {
    limit: 5,
    offset: 0,
    venueId: "5de65d89ec7e0e2f25045298",
    inVenue: true
};
const newCommunity = () => __awaiter(void 0, void 0, void 0, function* () {
    let getCommunity = new get_1.RequestGet(url, token);
    const resultWithOut = yield getCommunity.getWithoutParams();
    console.log('result newCommunity', resultWithOut);
});
const newUserList = () => __awaiter(void 0, void 0, void 0, function* () {
    let getUserList = new get_1.RequestGet(urlUserList, tokenUserList, paramUserList);
    const resultWith = yield getUserList.getWithParams();
    console.log('result User list', resultWith);
});
newCommunity();
newUserList();
